﻿using NUnit.Framework;
using RestSharp;
using System;
using System.Net;
using TechTalk.SpecFlow;

namespace Test2
{
    
    [Binding]
    public class SpecFlowFeature1Steps
    {
        
        [Given(@"Some have entered (.*) into the calculator")]
        public void GivenSomeHaveEnteredIntoTheCalculator(int p0)
        {
            //RestClient restClient = new RestClient("https://stackoverflow.com");
            //RestRequest restRequest = new RestRequest("/quest", Method.GET);
            RestClient restClient = new RestClient("https://stackoverflow.com");
            RestRequest restRequest = new RestRequest("/questions/56813919/rest-client-is-not-waiting-for-rest-api-service", Method.GET);
            Console.WriteLine("restRequest :  " + restRequest.ToString());
            RestResponse response = (RestResponse)restClient.Execute(restRequest);
            //RestRequest restRequest = new RestRequest("api/excelupload/Upload",Method.GET);
       
            int numericStatusCode = (int)response.StatusCode;

            Console.WriteLine("Response :  "+ numericStatusCode);
            Console.WriteLine("Content :  " + response.Content);
            Console.WriteLine("RawBytes :  " + response.RawBytes.ToString());


            // ScenarioContext.Current.Pending();
        }

        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            Console.WriteLine("Added");
            //ScenarioContext.Current.Pending();
        }

        
        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            p0 = 0;
            Console.WriteLine("Then");
            Assert.AreEqual(p0, 0);
            //ScenarioContext.Current.Pending();
        }
    }
}
