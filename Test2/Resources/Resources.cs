﻿using Microsoft.Office.Interop.Excel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


/*
 Created By : Peeranut Chungprasert
 Date :  14 of July 2019
 Purpose : Keep indivual function that related to User Interface for UI Testing.
 */
namespace Resources
{
    public class Resources 
    {
        IWebDriver driver;

        #region constructor
        /* Constructor if driver is a chrome browser */
        public Resources(ChromeDriver _driver,string url,TimeSpan timeSpan)
        {

            driver = _driver;
            /*Resize browser if it is not full screen*/
            driver.Manage().Window.Maximize();
            /* Implicit Wait is used to set timeout until testscript FindElement() before doing any action
               - This property/variable will affect to every FindElement() function.
            */
            if (timeSpan != null) { driver.Manage().Timeouts().ImplicitWait = timeSpan; }
            else { driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10); }        
            driver.Navigate().GoToUrl(url);  
        }

        /* Constructor if driver is a Firefox browser */
        public Resources(FirefoxDriver _driver, string url, TimeSpan timeSpan)
        {
            driver = _driver;
            /*Resize browser if it is not full screen*/
            driver.Manage().Window.Maximize();
            if (timeSpan != null) { driver.Manage().Timeouts().ImplicitWait = timeSpan; }
            else { driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10); }
            driver.Navigate().GoToUrl(url);
        }


        public Resources() { }
        #endregion

        #region  GetScreenShot
        public void GetScreenShot(string savePath,string name)
        {
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            ss.SaveAsFile(savePath+name+".jpg", ScreenshotImageFormat.Jpeg);
        }
        #endregion

        #region GetConsoleLog
        /* Validate Visualization data or other element that used JS */
        public void GetConsoleLog(string function,string input)
        {
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            /*eg.  example = js.ExecuteScript (return console.log(),DATA_COUNT).toString(); */
            string titles = js.ExecuteScript("return "+ function + ","+ input).ToString();
                Console.WriteLine(titles);
        }
        #endregion

        #region GetInnerText
        /* Get GetInnerHTMLByClass  eg. <div class='test'>Test </div>  OUTPUT--> "Test" */
        public void GetInnerHTMLByClass(string tagName)
        {           
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string title = (string)js.ExecuteScript("return document.getElementsByClassName('" + tagName + "').innerHTML");
            Console.WriteLine(title);
        }
        /* Get GetInnerHTMLByClass  eg. <div id='test'>Test </div>  OUTPUT--> "Test" */
        public void GetInnerHTMLById(string tagId)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string title = (string)js.ExecuteScript("return document.getElementsById('"+ tagId + "').innerHTML");
            Console.WriteLine(title);
        }
        /* Get GetInnerHTMLByClass  eg. <div name='test'>Test </div>  OUTPUT--> "Test" */
        public void GetInnerHTMLByName(string tagId)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string title = (string)js.ExecuteScript("return document.getElementsByName('" + tagId + "').innerHTML");
            Console.WriteLine(title);
        }

        #endregion


        #region Click
        public void Click(By by)
        {
            driver.FindElement(by).Click();
        }
        #endregion

        #region Type
        public void Type(By by, string text)
        {
            driver.FindElement(by).SendKeys(text);
        }
        #endregion

        #region CheckElementDisplay
        public bool CheckElementDisplay(By by)
        {
            bool result = driver.FindElement(by).Displayed;
            return result;
        }
        #endregion

        #region CheckElementEnable
        /*Eg. button checking */
        public bool CheckElementEnable(By by)
        {
            bool result = driver.FindElement(by).Enabled;
            return result;
        }
        #endregion

        #region CheckText
        public bool CheckText(By by,string expected)
        {
            bool result = !String.IsNullOrEmpty(driver.FindElement(by).Text);
            if(!String.IsNullOrEmpty(expected))
            {
                result = expected.Equals(driver.FindElement(by).Text);
            }         
            return result;
        }
        #endregion




        #region Excel


        public void CreateExcel(string path)
        {
            Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                Console.WriteLine("Excel is not properly installed!!");
                return;
            }
            Workbook xlWorkBook;
            Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Worksheet)xlWorkBook.Worksheets.get_Item(1);
            if (String.IsNullOrEmpty(path))
            {
                xlWorkBook.SaveAs("d:\\c-" + new Random().Next(1, 1000000000) + ".xls", XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            }
            else
            {
                xlWorkBook.SaveAs(path + @"\"+ new Random().Next(1, 1000000000) + ".xls", XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            }
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
        }

        public List<String> RetrieveExcelData(Range inputRng)
        {
            object[,] cellValues = (object[,])inputRng.Value2;
            List<String> lst = cellValues.Cast<object>().ToList().ConvertAll(x => Convert.ToString(x));
            return lst;
        }

        public List<String> ReadExcel(string filepath, string firstcell, string lastcell)
        {
            Application excel = new Application();
            Workbook workbook = excel.Workbooks.Open(filepath);
            Worksheet worksheet = workbook.Worksheets[1];
            Range range = worksheet.get_Range(firstcell, lastcell);
            object[,] cellValues = (object[,])range.Value2;
            /* RetrieveExcelData */
            List<String> lst = cellValues.Cast<object>().ToList().ConvertAll(x => Convert.ToString(x));
            /* Close Excel After Access File */
            workbook.Save();
            workbook.Close(0);
            excel.Quit();
            return lst;
        }

        public void WriteExcel(string filepath, string firstcell, string lastcell, string input)
        {
            
            Application excel = new Application();
            Workbook workbook = excel.Workbooks.Open(filepath);
            Worksheet worksheet = workbook.Worksheets[1];
            for (int i = 1; i < 9 ; i++)
            {
                worksheet.Cells[i,"A"] = input;
                //write the string cellValue into the text file
            }
            // worksheet.get_Range(firstcell,firstcell).Value2 = input;
            /* Close Excel After Access File */
            workbook.Save();
            workbook.Close(0);
            excel.Quit();
        }


        #endregion



       



        #region Login
        public void Login(string username, string password, string targetUrl, By fieldUserName, By fieldPassword,By submitLocation)
        {           
            driver.Navigate().GoToUrl(targetUrl);
            driver.FindElement(fieldUserName).SendKeys(username);
            driver.FindElement(fieldPassword).SendKeys(password);
            driver.FindElement(submitLocation).Click();
        }
        #endregion

      
    }

}
